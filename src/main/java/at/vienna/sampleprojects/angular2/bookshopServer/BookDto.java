package at.vienna.sampleprojects.angular2.bookshopServer;

public class BookDto {
    
    private Long id;
    private String name;
    private String size;
    private double price;
    private String image;
    private String description;
    private String category;
    
    public BookDto(Long id, String name, String size, double price, String image, String description, String category) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.price = price;
        this.image = image;
        this.description = description;
        this.category = category;
    }
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getSize() {
        return this.size;
    }
    
    public void setSize(String size) {
        this.size = size;
    }
    
    public double getPrice() {
        return this.price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public String getImage() {
        return this.image;
    }
    
    public void setImage(String image) {
        this.image = image;
    }   
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category=category;
  }   
}
