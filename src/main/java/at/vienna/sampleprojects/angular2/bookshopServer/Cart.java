package at.vienna.sampleprojects.angular2.bookshopServer;

import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties 
public class Cart {
    
  @JsonProperty("_content")
  private List<CartItem> content;
      
  public Cart() {
    this.content = new ArrayList();     
  }
  
  public void setContent(List<CartItem> content) {
    this.content = content;
  }
  
  public List<CartItem> getContent() {
    return content;
  }   
}