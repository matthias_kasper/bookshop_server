package at.vienna.sampleprojects.angular2.bookshopServer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartItem {

      @JsonProperty("_pizzaId")
      private int pizzaId; 
      @JsonProperty("_price")
      private double price;
      @JsonProperty("_image")
      private String image;
      @JsonProperty("_size")
      private String size;
      @JsonProperty("_name")
      private String name;
      @JsonProperty("_amount")
      private int amount;

    public CartItem() {
      
    } 
    
    public int getPizzaId() {
      return pizzaId;
    }

    public void setPizzaId(int pizzaId) {
      this.pizzaId = pizzaId;
    }

    public double getPrice() {
      return price;
    }

    public void setPrice(double price) {
      this.price = price;
    }

    public String getImage() {
      return image;
    }

    public void setImage(String image) {
      this.image = image;
    }

    public String getSize() {
      return size;
    }

    public void setSize(String size) {
      this.size = size;
    }
    
    public void setName(String name) {
      this.name = name;
    }
    
    public String getName() {
      return this.name;
    }

    public int getAmount() {
      return amount;
    }

    public void setAmount(int amount) {
      this.amount = amount;
    }   
  }