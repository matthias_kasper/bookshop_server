package at.vienna.sampleprojects.angular2.bookshopServer;

import org.springframework.http.HttpStatus;

public class ServerResponse {
  
  private HttpStatus statusCode;
  private int statusCodeValue; 
  private String statusMessage;

  public ServerResponse(HttpStatus statusCode, int statusCodeValue, String statusMessage) {
    this.statusCode = statusCode;
    this.statusCodeValue = statusCodeValue;
    this.statusMessage = statusMessage;
  }

  public HttpStatus getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(HttpStatus statusCode) {
    this.statusCode = statusCode;
  }

  public int getStatusCodeValue() {
    return statusCodeValue;
  }

  public void setStatusCodeValue(int statusCodeValue) {
    this.statusCodeValue = statusCodeValue;
  }

  public String getStatusMessage() {
    return statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }
}
