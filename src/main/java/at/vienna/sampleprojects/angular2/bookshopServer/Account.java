package at.vienna.sampleprojects.angular2.bookshopServer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Account {
  
  @JsonProperty("_id")
  private Long id;
  @JsonProperty("_salutation")
  private String salutation;
  @JsonProperty("_firstName")
  private String firstName;
  @JsonProperty("_lastName")
  private String lastName;
  @JsonProperty("_streetName")
  private String streetName;
  @JsonProperty("_cityName")
  private String cityName;
  @JsonProperty("_zipCode")
  private String zipCode;
  @JsonProperty("_userName")
  private String userName;
  @JsonProperty("_password")
  private String password;
  
  private AuthToken authToken;
  
  public Account() { this.id = new Long(0); }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
  public String getSalutation() {
    return salutation;
  }

  public void setSalutation(String salutation) {
    this.salutation = salutation;
  }
    
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  } 

  public AuthToken getAuthToken() {
    return authToken;
  }

  public void setAuthToken(AuthToken authToken) {
    this.authToken = authToken;
  }  
}
