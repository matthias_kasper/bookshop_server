package at.vienna.sampleprojects.angular2.bookshopServer;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

public class OrderHistory {
  
  private List<OrderHistoryItem> historyList;

  public OrderHistory() {
    this.historyList = new ArrayList<>();
  }
  public List<OrderHistoryItem> getHistoryList() {
    return historyList;
  }

  public void setHistoryList(List<OrderHistoryItem> historyList) {
    this.historyList = historyList;
  }
  
  public void addOrder(Cart cart, Long id) {
    try {
      if (cart != null) {
        Date orderDateTime = new Date();
        for (int i = 0; i < cart.getContent().size(); i++) {
          this.historyList.add(new OrderHistoryItem(id, cart.getContent().get(i), orderDateTime));
        }
      }
    } catch(Exception exp) {
      System.out.println("Error in OrderHistory::addOrder -> " + exp);
    }
  }    
  
  public List<OrderHistoryItem> getOrderHistory(Long id) {
    List<OrderHistoryItem> history = new ArrayList<>();
    for (OrderHistoryItem orderHistoryItem : this.historyList) {
      if (orderHistoryItem.getCustomerId().longValue() == id.longValue()) {
        history.add(orderHistoryItem);
      }
    }
    
    return history;
  }
}