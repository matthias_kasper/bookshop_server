package at.vienna.sampleprojects.angular2.bookshopServer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserAuthDto {
  
  @JsonProperty("_userName")
  private String userName;
  @JsonProperty("_password")
  private String password; 

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  } 
}