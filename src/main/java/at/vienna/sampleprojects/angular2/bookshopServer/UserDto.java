package at.vienna.sampleprojects.angular2.bookshopServer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDto {
  
  @JsonProperty("_id")
  private Long id;
  @JsonProperty("_salutation")
  private String salutation;
  @JsonProperty("_firstName")
  private String firstName;
  @JsonProperty("_lastName")
  private String lastName;
  @JsonProperty("_streetName")
  private String streetName;
  @JsonProperty("_cityName")
  private String cityName;
  @JsonProperty("_zipCode")
  private String zipCode;
    
  private AuthToken authToken;
  
  public UserDto() { }
  
  public UserDto(Account account) {
    this.id = account.getId();
    this.salutation = account.getSalutation();
    this.firstName = account.getFirstName();
    this.lastName = account.getLastName();
    this.streetName = account.getStreetName();
    this.cityName = account.getCityName();
    this.zipCode = account.getZipCode(); 
    this.authToken = new AuthToken(
            account.getAuthToken().getSuccess(), 
            account.getAuthToken().getToken());
  }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
  public String getSalutation() {
    return salutation;
  }

  public void setSalutation(String salutation) {
    this.salutation = salutation;
  }
    
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  } 

  public AuthToken getAuthToken() {
    return authToken;
  }

  public void setAuthToken(AuthToken authToken) {
    this.authToken = authToken;
  }  
}