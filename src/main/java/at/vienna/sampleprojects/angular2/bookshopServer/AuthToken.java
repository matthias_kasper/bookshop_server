package at.vienna.sampleprojects.angular2.bookshopServer;

public class AuthToken {
  private final boolean success;
  private final String token;
  
  public AuthToken(boolean success, String token) {
    this.success = success;
    this.token = token;
  }
  
  public String getToken() {
    return this.token;
  }
  
  public boolean getSuccess() {
    return this.success;
  }
}
