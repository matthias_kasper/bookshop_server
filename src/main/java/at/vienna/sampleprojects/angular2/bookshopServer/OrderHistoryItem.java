package at.vienna.sampleprojects.angular2.bookshopServer;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class OrderHistoryItem {
  
  @JsonProperty("_customerId")
  private Long customerId;
  @JsonProperty("_item")
  private CartItem cartItem;
  @JsonProperty("_date")
  private Date orderDateTime;

  public OrderHistoryItem(Long customerId, CartItem cartItem, Date orderDateTime) {
    this.customerId = customerId;
    this.cartItem = cartItem;
    this.orderDateTime = orderDateTime;
  }
  
  public Long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  public CartItem getCartItem() {
    return cartItem;
  }

  public void setCartItem(CartItem cartItem) {
    this.cartItem = cartItem;
  }

  public Date getOrderDateTime() {
    return orderDateTime;
  }

  public void setOrderDateTime(Date orderDateTime) {
    this.orderDateTime = orderDateTime;
  }  
}
