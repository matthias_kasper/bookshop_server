package at.vienna.sampleprojects.angular2.bookshopServer;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import javax.ws.rs.core.MediaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.security.SecureRandom;
import java.math.BigInteger;
import org.apache.commons.lang3.Validate;

@RestController
@EnableAutoConfiguration
public class Server {

	private final List<BookDto> bookList;
	private final Set<Account> accounts;
	private final OrderHistory orderHistory;

	public Server() {
		this.bookList = createBookList();
		this.accounts = new HashSet<>();
		this.orderHistory = new OrderHistory();
	}

	@RequestMapping("/")
	String home() {
		return "Bookshop-Server up and running!";
	}

	@CrossOrigin
	@RequestMapping(value = "/http/bookdetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object doGetBookDetails(@RequestParam(value = "id", required = true) Long id) {
		if (id == null) {
			return new ServerResponse(HttpStatus.BAD_REQUEST, 400, "Ungültiger Parameter: id");
		}
		System.out.println(" -> (HTTP) Anfrage zur Übermittlung von Book-Details (id: " + id + ") erhalten:");
		BookDto dto = null;
		try {
			dto = getBookDetails(id);
			System.out.println("Objekt gefunden.");
			System.out.println("Antwort wird gesendet.");
		} catch (IllegalArgumentException exp) {
			System.out
					.println("Antwort kann nicht gesendet werden. Keine Datenen verfügbar: " + exp.getMessage());
		}

		return dto;
	}

	@RequestMapping(value = "/jsonp/bookdetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object getBookDetails(@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "callback", required = true) String jsonpCallback) {
		System.out.println(" -> (Jsonp) Anfrage zur Übermittlung von Book-Details (id: " + id + ") erhalten:");
		BookDto dto = null;
		try {
			dto = getBookDetails(id);
			System.out.println("Objekt gefunden.");
			System.out.println("Antwort wird gesendet.");
		} catch (IllegalArgumentException exp) {
			System.out
					.println("Antwort kann nicht gesendet werden. Keine Daten verfügbar: " + exp.getMessage());
		}

		return convertToJasonP(dto, jsonpCallback);
	}

	@CrossOrigin
	@RequestMapping(value = "/http/booklist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object doGetBookList(
			@RequestParam(value = "bookType", required = false, defaultValue = "na") String bookType) {

		System.out.print(" -> (Http) Anfrage zur Übermittlung des Angebots erhalten ");
		switch (bookType) {
		case "L": {
			System.out.println("Jack London");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("L")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return filterList;
		}
		case "V": {
			System.out.println("Jules Verne");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("V")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return filterList;
		}
		case "M": {
			System.out.println("Thomas Mann");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("M")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return filterList;
		}
		case "B": {
			System.out.println("Edgar Brau");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("B")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return filterList;
		}
		default: {
			System.out.println("Alle Autoren");
			System.out.println(this.bookList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return this.bookList;
		}
		}
	}

	@RequestMapping(value = "/jsonp/booklist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object getBookList(
			@RequestParam(value = "bookType", required = false, defaultValue = "na") String bookType,
			@RequestParam(value = "callback", required = true) String jsonpCallback) {
		System.out.print(" -> (Jsonp) Anfrage zur Übermittlung des Book-Angebots erhalten ");
		switch (bookType) {
		case "L": {
			System.out.println("Jack London");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("L")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return convertToJasonP(filterList, jsonpCallback);
		}
		case "V": {
			System.out.println("Jules Verne");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("V")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return convertToJasonP(filterList, jsonpCallback);
		}
		case "M": {
			System.out.println("Thomas Mann");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("M")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return convertToJasonP(filterList, jsonpCallback);
		}
		case "B": {
			System.out.println("Edgar Brau");
			List<BookDto> filterList = new ArrayList<>();
			for (BookDto dto : this.bookList) {
				if (dto.getCategory().equals("B")) {
					filterList.add(dto);
				}
			}
			System.out.println(filterList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return convertToJasonP(filterList, jsonpCallback);
		}
		default: {
			System.out.println("Alle Bücher anzeigen");
			System.out.println(this.bookList.size() + " Objekte gefunden");
			System.out.println("Antwort wird gesendet.");
			return convertToJasonP(this.bookList, jsonpCallback);
		}
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/http/checkout", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody Object doCheckout(@RequestBody CheckoutDto checkout) {

		try {
			Validate.notNull(checkout);
			validateCart(checkout.getCart());
			Account account = null;
			if (checkout.getToken() != null && checkout.getToken().length() == 0) {
				System.out.println(" -> Order von Gast erhalten: ");
			} else if (checkout.getToken() != null && checkout.getToken().length() > 0) {
				account = findAccountByToken(checkout.getToken());
				if (account != null) {
					System.out.println(" -> (Http) Order von registiertem User erhalten (KD-NR: "
							+ account.getId() + ")");
				} else {
					System.out.println(" -> (Http) Order von registiertem User erhalten: ");
				}
			} else {
				System.out.println(" -> (Http) Order erhalten");
			}
			for (int i = 0; i < checkout.getCart().getContent().size(); i++) {
				System.out.println("Position " + (i + 1) + ":" + checkout.getCart().getContent().get(i).getAmount()
						+ "x " + checkout.getCart().getContent().get(i).getName());
			}
			if (account != null) {
				validateAccount(account, false);
				this.orderHistory.addOrder(checkout.getCart(), account.getId());
				System.out.println("Lieferung geht an: Kunden-Nummer: " + account.getId() + " - "
						+ account.getFirstName() + " " + account.getLastName() + ", " + account.getCityName());
			} else if (checkout.getAccount() != null) {
				System.out.println(
						"Lieferung geht an: Kunden-Nummer: keine Angabe - " + checkout.getAccount().getFirstName() + " "
								+ checkout.getAccount().getLastName() + ", " + checkout.getAccount().getCityName());
			}
			System.out.println("Order wird ausgeführt.");
			return new ServerResponse(HttpStatus.OK, 200, "Order successfully transfered.");
		} catch (IllegalArgumentException exp) {
			System.out.println("Order wird kann nicht ausgeführt werden: " + exp.getMessage());
			return new ServerResponse(HttpStatus.CONFLICT, 409, exp.getMessage());
		} catch (Exception exp) {
			System.out.println("Order kann nicht ausgeführt werden: " + exp.getMessage());
			return new ServerResponse(HttpStatus.INTERNAL_SERVER_ERROR, 500, exp.getMessage());
		}
	}

	@RequestMapping(value = "/jsonp/checkout", method = RequestMethod.GET)
	public @ResponseBody Object checkout(@RequestParam(value = "checkout", required = true) String checkout,
			@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "callback", required = true) String jsonpCallback) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			CheckoutDto checkoutDto = mapper.readValue(checkout, CheckoutDto.class);

			Validate.notNull(checkoutDto);
			validateCart(checkoutDto.getCart());
			Account account = null;
			if (checkoutDto.getToken() != null && checkoutDto.getToken().length() == 0) {
				System.out.println(" -> Order von Gast erhalten: ");
			} else if (checkoutDto.getToken() != null && checkoutDto.getToken().length() > 0) {
				account = findAccountByToken(checkoutDto.getToken());
				if (account != null) {
					System.out.println(" -> (Jsonp) Order von registiertem User erhalten (KD-NR: "
							+ account.getId() + ")");
				} else {
					System.out.println(" -> (Jsonp) Order von registiertem User erhalten: ");
				}
			} else {
				System.out.println(" -> (Jsonp) Order erhalten");
			}
			for (int i = 0; i < checkoutDto.getCart().getContent().size(); i++) {
				System.out.println("Position " + (i + 1) + ":" + checkoutDto.getCart().getContent().get(i).getAmount()
						+ "x " + checkoutDto.getCart().getContent().get(i).getName());
			}
			if (account != null) {
				validateAccount(account, false);
				this.orderHistory.addOrder(checkoutDto.getCart(), account.getId());
				System.out.println("Lieferung geht an: Kunden-Nummer: " + account.getId() + " - "
						+ account.getFirstName() + " " + account.getLastName() + ", " + account.getCityName());
			} else if (checkoutDto.getAccount() != null) {
				System.out.println("Lieferung geht an: Kunden-Nummer: keine Angabe - "
						+ checkoutDto.getAccount().getFirstName() + " " + checkoutDto.getAccount().getLastName() + ", "
						+ checkoutDto.getAccount().getCityName());
			}
			System.out.println("Order wird ausgeführt.");
			return convertToJasonP(new ServerResponse(HttpStatus.OK, 200, "Order successfully transfered."),
					jsonpCallback);
		} catch (IllegalArgumentException exp) {
			System.out.println("Order wird kann nicht ausgeführt werden: " + exp.getMessage());
			return convertToJasonP(new ServerResponse(HttpStatus.CONFLICT, 409, exp.getMessage()), jsonpCallback);
		} catch (Exception exp) {
			System.out.println("Order kann nicht ausgeführt werden: " + exp.getMessage());
			return convertToJasonP(new ServerResponse(HttpStatus.INTERNAL_SERVER_ERROR, 500, exp.getMessage()),
					jsonpCallback);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/http/login", method = RequestMethod.POST, consumes = "application/json", produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object doLogin(@RequestBody UserAuthDto userAuthDto) {
		try {
			Validate.notNull(userAuthDto);
			System.out.println(" -> (Http) Login-Anfrage für User '" + userAuthDto.getUserName() + "' erhalten:");
			UserDto userDto = this.validateLogin(userAuthDto.getUserName(), userAuthDto.getPassword());
			if (userDto != null) {
				System.out.println("Zugang genehmigt.");
				return userDto;
			} else {
				Account invalid = new Account();
				invalid.setAuthToken(new AuthToken(false, null));
				System.out.println("Zugang verweigert.");
				return invalid;
			}
		} catch (IllegalArgumentException exp) {
			System.out.println("Zugang verweigert: " + exp.getMessage());
			return new ServerResponse(HttpStatus.PARTIAL_CONTENT, 206, exp.getMessage());
		} catch (Exception exp) {
			System.out.println("Zugang verweigert: " + exp.getMessage());
			return new ServerResponse(HttpStatus.PARTIAL_CONTENT, 206, exp.getMessage());
		}
	}

	// Input:
	@RequestMapping(value = "/jsonp/login", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object login(@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "passwd", required = true) String passwd,
			@RequestParam(value = "callback", required = true) String jsonpCallback) {

		System.out.println(" -> (Jsonp) Login-Anfrage für User '" + email + "' erhalten:");
		try {
			UserDto userDto = this.validateLogin(email, passwd);
			if (userDto != null) {
				System.out.println("Zugang genehmigt.");
				return convertToJasonP(userDto, jsonpCallback);
			} else {
				Account invalid = new Account();
				invalid.setAuthToken(new AuthToken(false, null));
				System.out.println("Zugang verweigert.");
				return convertToJasonP(invalid, jsonpCallback);
			}
		} catch (IllegalArgumentException exp) {
			System.out.println("Zugang verweigert: " + exp.getMessage());
			return convertToJasonP(new ServerResponse(HttpStatus.PARTIAL_CONTENT, 206, exp.getMessage()),
					jsonpCallback);
		} catch (Exception exp) {
			System.out.println("Zugang verweigert: " + exp.getMessage());
			return convertToJasonP(new ServerResponse(HttpStatus.PARTIAL_CONTENT, 206, exp.getMessage()),
					jsonpCallback);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/http/createaccount", method = RequestMethod.POST)
	public @ResponseBody Object doCreateAccount(@RequestBody Account account) {
		try {
			System.out.println(" -> (Http) Anfrage zum Erstellen eines Userkontos für User '"
					+ account.getUserName() + "' erhalten:");
			try {
				this.createAccount(account);
				System.out.println("Userkonto erstellt.");
				return new ServerResponse(HttpStatus.OK, 200, "Account successfully created.");
			} catch (IllegalArgumentException exp) {
				System.out.println("Userkonto konnte nicht erstellt werden. " + exp.getMessage());
				return new ServerResponse(HttpStatus.CONFLICT, 409, exp.getMessage());
			}
		} catch (Exception exp) {
			System.out.println("Unbekannter Fehler beim Erstellen des Userkontos: " + exp.getMessage());
			return new ServerResponse(HttpStatus.INTERNAL_SERVER_ERROR, 500, exp.getMessage());
		}

	}

	@RequestMapping(value = "/jsonp/createaccount", method = RequestMethod.GET)
	public @ResponseBody Object createAccount(@RequestParam(value = "json", required = true) String json,
			@RequestParam(value = "callback", required = true) String jsonpCallback) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Account account = mapper.readValue(json, Account.class);
			System.out.println(" -> (Jsonp) Anfrage zum Erstellen eines Userkontos für User '"
					+ account.getUserName() + "' erhalten:");
			try {
				this.createAccount(account);
				System.out.println("Userkonto erstellt.");
				return convertToJasonP(new ServerResponse(HttpStatus.OK, 200, "Account successfully created."),
						jsonpCallback);
			} catch (IllegalArgumentException exp) {
				System.out.println("Userkonto konnte nicht erstellt werden. " + exp.getMessage());
				return convertToJasonP(new ServerResponse(HttpStatus.CONFLICT, 409, exp.getMessage()), jsonpCallback);
			}
		} catch (Exception exp) {
			System.out.println("Unbekannter Fehler beim Erstellen des Userkontos: " + exp.getMessage());
			return convertToJasonP(new ServerResponse(HttpStatus.INTERNAL_SERVER_ERROR, 500, exp.getMessage()),
					jsonpCallback);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/http/historyorderlist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object getOrderList(@RequestParam(value = "token", required = true) String token) {
		Long customerId = getCustomerIdByToken(token);
		if (customerId == null) {
			System.out.println(" -> (Http) Anfrage zur Übermittlung der Bestellhistorie.");
			System.out.println("Das Kundenkonto konnte nicht gefunden werden");
			return new ServerResponse(HttpStatus.CONFLICT, 409, "Das Kundenkonto konnte nicht gefunden werden.");
		}
		List<OrderHistoryItem> orderHistoryList = new ArrayList<>();
		System.out.println(" -> (Http) Anfrage zur Übermittlung der Bestellhistorie für KD-NR: " + customerId);
		orderHistoryList = this.orderHistory.getOrderHistory(customerId);
		System.out.println(orderHistoryList.size() + " Objekte gefunden");
		System.out.println("Antwort wird gesendet.");
		return orderHistoryList;
	}

	@RequestMapping(value = "/jsonp/historyorderlist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody Object getOrderList(@RequestParam(value = "callback", required = true) String jsonpCallback,
			@RequestParam(value = "token", required = true) String token) {
		System.out.println("token: " + token);
		Long customerId = getCustomerIdByToken(token);
		if (customerId == null) {
			System.out.println(" -> (Jsonp) Anfrage zur Übermittlung der Bestellhistorie:");
			System.out.println("Das Kundenkonto konnte nicht gefunden werden");
			return convertToJasonP(
					new ServerResponse(HttpStatus.CONFLICT, 409, "Das Kundenkonto konnte nicht gefunden werden."),
					jsonpCallback);
		}

		List<OrderHistoryItem> orderHistoryList = new ArrayList<>();
		System.out.println(" -> (Jsonp) Anfrage zur Übermittlung der Bestellhistorie für KD-NR: " + customerId);
		orderHistoryList = this.orderHistory.getOrderHistory(customerId);
		System.out.println(orderHistoryList.size() + " Objekte gefunden");
		System.out.println("Antwort wird gesendet.");
		return convertToJasonP(orderHistoryList, jsonpCallback);
	}

	private String convertToJasonP(Object o, String jsonpCallback) {
		String outputmessage = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			outputmessage = mapper.writeValueAsString(o);
		} catch (Exception e) {
			System.out.println("ERROR: " + e);
		}
		if (outputmessage != null) {
			outputmessage = jsonpCallback + "(" + outputmessage + ")";
		}

		return outputmessage;
	}

	private Long getCustomerIdByToken(String token) {
		if (token != null) {
			for (Account account : this.accounts) {
				if (account != null && account.getAuthToken() != null && account.getAuthToken().getToken() != null) {
					if (account.getAuthToken().getToken().equals(token)) {
						return account.getId();
					}
				}
			}
		}
		return null;
	}

	private void createAccount(Account in) throws IllegalArgumentException {
		if (("user").equalsIgnoreCase(in.getUserName())) {
			throw new IllegalArgumentException(
					"Ein User mit dem Namen 'user' darf nicht erstellt werden. Bitte verwende einen anderen Usernamen.");
		}
		try {
			this.validateAccount(in, true);
		} catch (NullPointerException exp) {
			throw new IllegalArgumentException(exp.getMessage());
		}
		Iterator it = this.accounts.iterator();
		Long maxId = 0L;
		while (it.hasNext()) {
			Account value = (Account) it.next();
			if (value.getUserName().equals(in.getUserName())) {
				throw new IllegalArgumentException("Der User existiert bereits.");
			}
			if (value != null && value.getId() > maxId) {
				maxId = value.getId();
			}
		}

		in.setId(maxId + 1);
		this.accounts.add(in);
	}

	private AuthToken createAuthToken() {
		return new AuthToken(true, createRandomString());
	}

	private String createRandomString() {

		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}

	private UserDto validateLogin(String username, String password) throws IllegalArgumentException, Exception {
		Validate.notNull(username);
		Validate.notNull(password);
		Iterator it = this.accounts.iterator();
		while (it.hasNext()) {
			Account value = (Account) it.next();
			if (value.getUserName().equalsIgnoreCase(username)) {
				if (value.getPassword().equals(password)) {
					value.setAuthToken(this.createAuthToken());
					UserDto userDto = new UserDto(value);
					return userDto;
				}
				return null;
			}
		}
		return null;
	}

	private void validateCart(Cart cart) throws IllegalArgumentException {
		try {
			Validate.notNull(cart, "Die Order wurde nicht korrekt übertragen.");
			Validate.notNull(cart.getContent(), "Der Warenkorb wurde nicht korrekt übertragen.");
			Validate.isTrue(cart.getContent().size() > 0, "Der Warenkorb hat keinen Inhalt.");
		} catch (NullPointerException exp) {
			throw new IllegalArgumentException(exp);
		}
	}

	private void validateAccount(Account in, boolean auth) throws NullPointerException, IllegalArgumentException {
		Validate.notNull(in, "Keine Userinformationen gefunden.");
		Validate.notBlank(in.getSalutation(), "Fehlende Daten: Anrede");
		if (auth) {
			Validate.notBlank(in.getUserName(), "Fehlende Daten: Username");
			Validate.notBlank(in.getPassword(), "Fehlende Daten: Passwort");
		}
		Validate.notBlank(in.getFirstName(), "Fehlende Daten: Vorname");
		Validate.notBlank(in.getLastName(), "Fehlende Daten: Nachname");
		Validate.notBlank(in.getStreetName(), "Fehlende Daten: Strasse");
		Validate.notBlank(in.getCityName(), "Fehlende Daten: Ort");
		Validate.notBlank(in.getZipCode(), "Fehlende Daten: PLZ");
		Validate.notBlank(in.getSalutation(), "Fehlende Daten: Anrede");
	}

	private Account findAccountByToken(String token) {
		for (Account account : this.accounts) {
			if (account != null && account.getAuthToken() != null) {
				if (account.getAuthToken().getToken().equals(token)) {
					return account;
				}
			}
		}
		System.out.println("Fehler: Zum übermittelten Token konnte kein Userkonto gefunden werden.");
		return null;
	}

	private List<BookDto> createBookList() {
		List<BookDto> list = new ArrayList<>();
		list.add(createBook(100L, "A Son of the Sun", "Jack London", 9.99, "ASonoftheSun.png",
				"A Son of the Sun is a 1912 novel by Jack London. It is set in the South Pacific at the beginning of the 20th century "
						+ "and consists of eight separate stories. "
						+ "David Grief is a forty-year-old English adventurer who came to the South seas years ago and became rich. ",
				"L"));
		list.add(createBook(101L, "A journey to the center", "Jules Verne", 9.99,
				"A_Journey_to_the_Centre_of_the_Earth.png",
				"After descending into the crater, the three travellers set off into the bowels of the Earth, encountering many strange phenomena and great dangers,"
						+ " including a chamber filled with firedamp, and steep-sided wells around the path."
						+ " After taking a wrong turn, they run out of water and Axel almost dies, but Hans taps into a neighbouring subterranean river. ",
				"V"));
		list.add(createBook(102L, "Casablanca", "Edgar Brau", 7.49, "Casablanca.png",
				"In December 1941, American expatriate Rick Blaine owns an upscale nightclub and gambling den in Casablanca."
						+ " Rick's Café Américain attracts a varied clientele, including Vichy French and German officials, "
						+ "refugees desperate to reach the still-neutral United States,"
						+ "and those who prey on them. Although Rick professes to be neutral in all matters, "
						+ "he ran guns to Ethiopia during its war with Italy and fought on the Loyalist side in the Spanish Civil War.",
				"B"));
		list.add(createBook(103L, "Clovis Dardentor", "Jules Verne", 25.79, "Clovis_Dardentor.png",
				"The novel tells the story of two cousins, Jean Taconnat and Marcel Lornans, travelling from Cette, France to Oran, "
						+ "Algeria, with the purpose of enlisting in the 5th regiment of the Chasseurs D'Afrique."
						+ "On board the Argelès, the ship to Oran, they meet Clovis Dardentor, a wealthy industrialist, who is the central character of the novel. ",
				"V"));
		list.add(createBook(104L, "Der kleine Herr Friedmann", "Thomas Mann", 12.79, "Der_kleine_Herr_Friedemann.png",
				"Die Novelle erzählt in fünfzehn knappen Kapiteln die Lebensgeschichte von Johannes Friedemann, der als Kleinkind vom Wickeltisch fiel und seitdem an einer körperlichen Missbildung leidet. ", "M"));
		list.add(createBook(105L, "Doktor Faustus", "Thomas Mann", 7.79, "Doktor_Faustus.png",
				"Doktor Faustus erzählt das Leben des Komponisten Adrian Leverkühn aus der rückblickenden Perspektive seines Freundes Serenus Zeitblom, der mit der Biographie am 23. Mai 1943 zu schreiben beginnt "
				+ "und in seine Aufzeichnungen über Leverkühns Lebensweg und seine Produktionen immer wieder Berichte und Kommentare zu den Ereignissen der Kriegsjahre 1943 bis 1945 einfließen lässt. ", "M"));
		list.add(createBook(106L, "Gerhart Hauptmenn", "Thomas Mann", 10.49, "Gerhart_Hauptmenn.png", "", "M"));
		list.add(createBook(108L, "The mysterious island", "Jules Verne", 12.99, "Mysterious_Island.png", 
				"The plot focuses on the adventures of five Americans on an uncharted island in the South Pacific." +
				"During the American Civil War, five northern prisoners of war decide to escape, during the siege of Richmond, Virginia, by hijacking a balloon.", "V"));
		list.add(createBook(109L, "The children of Captain Grant", "Jules Verne", 11.99,
				"The_Children_of_Captain_Grant.png", 
				"The contents of a shark's stomach contain a bottle that holds notes written in three different languages. Much of the notes are indecipherable; however,"+
			 " together they may reveal the location of the whereabouts of Captain Harry Grant, whose ship the Britannia was lost over two years ago.", "V"));
		list.add(createBook(110L, "Tod in Venedig", "Thomas Mann", 25.49, "Tod_in_Venedig.png", 
				"Der alternde Schriftsteller Gustav von Aschenbach wird von einer Schaffenskrise heimgesucht und beschließt, zur Erholung eine Reise nach Venedig anzutreten.", "M"));
		return list;
	}

	private BookDto createBook(Long id, String name, String desc, double price, String image, String description,
			String category) {
		return new BookDto(id, name, desc, price, image, description, category);
	}

	private BookDto getBookDetails(Long id) throws IllegalArgumentException {
		for (BookDto book : this.bookList) {
			if (book != null && book.getId().longValue() == id.longValue()) {
				return book;
			}
		}

		throw new IllegalArgumentException("Book mit ID " + id + " unbekannt.");
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Server.class, args);
	}
}
