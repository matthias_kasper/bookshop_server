package at.vienna.sampleprojects.angular2.bookshopServer;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckoutDto {
  
  @JsonProperty("_cart")
  private Cart cart;
  @JsonProperty("_token")
  private String token;  
  @JsonProperty("_account")
  private Account account;
    
  public Cart getCart() {
    return this.cart;
  }

  public void setCart(Cart cart) {
    this.cart = cart;
  }   

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }  
  
  public Account getAccount() {
    return this.account;
  }
  
  public void setAccount(Account account) {
    this.account = account;
  }
}
